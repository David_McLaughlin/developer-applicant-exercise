This refactoring example is a ruby port of a refactoring exercise from
_Extreme Programming Explored_.  template.rb taken from the the Ruby
refactoring code https://github.com/kevinrutherford/rrwb-code

## First inspection   
On first inspection it looked like the template function's job was to find and replace two tokens, `%CODE%` and `%ALTCODE%`, like some kind of mailmerge. The value of `%ALTCODE%` is computed from the `req_id` parameter directly in the `template` function. And the code to do the find-and-replace operation is overly verbose and only replaces a single instance of each token.  

## Refactoring  
First thing I did was add some logic to ensure the `source_template` variable was a string. `req_id` can be another type of object as long as we can coerce it into a string, but I figured at the very least requiring `source_template` to be a string was a good idea.  

The next edge case we want to handle is if the `source_template` parameter is frozen. This is as simple as copying it into a new string and operating on that.  

I also added a second value parameter called `alt_code`. This was being computed directly in the `template` function previously but that was a bit of a code smell. The `template` function should only be responsible for building the final template, it shouldn't necessarily be doing transforms on the input to fill in the template values. Ideally if `alt_code` is something that is always computable from `req_id` then we'd want to keep that logic in a single place so if it ever changes or needs to be reviewed it's not spread out throughout the codebase.  

Finally I used gsub to do the find and replace of our target tokens.

## Tests  
For the tests around the template function my goal was to define the success and failure cases as best as possible. Now ideally if the behavior of template changes one of our tests will start failing.   