module Template
  def template(source_template, req_id, alt_code)
    if !source_template.is_a?(String)
      raise "Parameter source_template must be a string"
    end

    if source_template.frozen?
      template = String.new(source_template)
      template_when_safe template, req_id, alt_code
    else
      template_when_safe source_template, req_id, alt_code
    end
  end

  private
  <<-DOC
    Private function that operates with the assumptions that the input source_template
    is a string that is not frozen.
  DOC
  def template_when_safe(source_template, req_id, alt_code)
    source_template.gsub /%CODE%|%ALTCODE%/, '%CODE%' => req_id, '%ALTCODE%' => alt_code
  end
end