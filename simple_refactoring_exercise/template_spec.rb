require_relative 'template'

describe Template do
  include Template

  code = "12345000"
  alt_code = "#{code[0..4]}-#{code[5..-1]}"

  #
  # Expect no change cases
  #
  it "should not change the template" do
    template_text = "There is no code or alt_code"
    expected_text = template_text
    actual_text   = template(template_text, code, alt_code)

    expect(actual_text).to eq expected_text
  end

  it "should not replace %code% the template" do
    template_text = "Code hasn't been replaced %code%"
    expected_text = template_text
    actual_text   = template(template_text, code, alt_code)

    expect(actual_text).to eq expected_text
  end

  it "should not replace %altcode% the template" do
    template_text = "AltCode hasn't been replaced %altcode%"
    expected_text = "AltCode hasn't been replaced %altcode%"
    actual_text   = template(template_text, code, alt_code)

    expect(actual_text).to eq expected_text
  end

  #
  # %CODE% replacement cases
  #
  it "should substitute %CODE% in the template" do
    template_text = "Code is now %CODE%"
    expected_text = "Code is now #{code}"
    actual_text   = template(template_text, code, alt_code)

    expect(actual_text).to eq expected_text
  end

  <<-DOC
    This test covers two things, multiple replacement tokens present in the template_text
    and replacement tokens that have no separation. Normally these cases should be broken into
    thier own tests, but in this case the logic is simple enough that this should be fine
  DOC
  it "should substitute every instance of %CODE% in the template" do
    template_text = "Code is now %CODE%, %CODE%%CODE%, %CODE%"
    expected_text = "Code is now #{code}, #{code}#{code}, #{code}"
    actual_text   = template(template_text, code, alt_code)

    expect(actual_text).to eq expected_text
  end

  it "should substitute every instance of %CODE% in a multi-line template" do
    template_text = %{
      %CODE% = %CODE%
      Here's a second line with no code
      And one with an alt_code %CODE%
      And another without
    }
    expected_text = %{
      #{code} = #{code}
      Here's a second line with no code
      And one with an alt_code #{code}
      And another without
    }
    actual_text   = template(template_text, code, alt_code)

    expect(actual_text).to eq expected_text
  end

  it "should not substitute %CODE% when split over multiple lines" do
    template_text = %{
      %CODE
      %
      trailing
    }
    expected_text = template_text
    actual_text   = template(template_text, code, alt_code)

    expect(actual_text).to eq expected_text
  end

  #
  # %ALTCODE% replacement cases
  #
  it "should substitute %ALTCODE% in the input template" do
    template_text = "AltCode is now %ALTCODE%"
    expected_text = "AltCode is now #{alt_code}" 
    actual_text   = template(template_text, code, alt_code)

    expect(actual_text).to eq expected_text
  end

  
  <<-DOC
    This test covers two things, multiple replacement tokens present in the template_text
    and replacement tokens that have no separation. Normally these cases should be broken into
    thier own tests, but in this case the logic is simple enough that this should be fine
  DOC
  it "should substitute every instance of %ALTCODE% in the template" do
    template_text = "AltCode is now %ALTCODE%, %ALTCODE%%ALTCODE%, %ALTCODE%..."
    expected_text = "AltCode is now #{alt_code}, #{alt_code}#{alt_code}, #{alt_code}..."
    actual_text   = template(template_text, code, alt_code)

    expect(actual_text).to eq expected_text
  end

  it "should substitute every instance of %ALTCODE% in a multi-line template" do
    template_text = %{
      %ALTCODE% = %ALTCODE%
      Here's a second line with no code
      And one with an alt_code %ALTCODE%
      And another without
    }
    expected_text = %{
      #{alt_code} = #{alt_code}
      Here's a second line with no code
      And one with an alt_code #{alt_code}
      And another without
    }
    actual_text   = template(template_text, code, alt_code)

    expect(actual_text).to eq expected_text
  end

  it "should not substitute %ALTCODE% when split over multiple lines" do
    template_text = %{
      %ALTCODE
      %
      trailing
    }
    expected_text = template_text
    actual_text   = template(template_text, code, alt_code)

    expect(actual_text).to eq expected_text
  end

  #
  # %CODE% and %ALTCODE% replacement cases
  #
  it "should substitute both %CODE% and %ALTCODE% in the template" do
    template_text = "Code is %CODE% and AltCode is %ALTCODE%"
    expected_text = "Code is #{code} and AltCode is #{alt_code}"
    actual_text   = template(template_text, code, alt_code)

    expect(actual_text).to eq expected_text
  end

  it "should substitute every instance of %CODE% and %ALTCODE% in the template" do
    template_text = "Code is %CODE%%CODE% %CODE% and AltCode is %ALTCODE% %ALTCODE%%ALTCODE% with trailing text"
    expected_text = "Code is #{code}#{code} #{code} and AltCode is #{alt_code} #{alt_code}#{alt_code} with trailing text"
    actual_text   = template(template_text, code, alt_code)

    expect(actual_text).to eq expected_text
  end

  it "should substitute every instance of %CODE% and %ALTCODE% in a multi-line template" do
    template_text = %{
      %CODE% != %ALTCODE%
      Here's a second line with no code
      And one with an alt_code %CODE%
      And another without
      %ALTCODE%
    }
    expected_text = %{
      #{code} != #{alt_code}
      Here's a second line with no code
      And one with an alt_code #{code}
      And another without
      #{alt_code}
    }
    actual_text   = template(template_text, code, alt_code)

    expect(actual_text).to eq expected_text
  end

end