module Liquid
    module Tags
        class RenderRawTag < Liquid::Tag
            def initialize(tag_name, params, options)
                super
                @name = (params.gsub "'", "").gsub " ", ""
            end

            def render(context)
                file_system = (context.registers[:file_system] ||= Liquid::Template.file_system)
                file_system.read_template_file(@name)
            end
        end
    end
end