require 'httparty'
require 'json'
require 'ruby-limiter'

class RedditAPI
    extend Limiter::Mixin

    # Reddit limits to 60 requests per second, we'll use 50 to be on the safe side.
    # This is a pretty naive rate limiting approach, we just block if requests are happening
    # faster than 1 per (60 / rate:) seconds. For this example project it should be fine,
    # for a production case we could return cached responses, we could return a rate 
    # limit warning and handle it clientside by waiting and retrying, etc.
    limit_method :hot, rate: 50

    def hot subreddit, max_results
        url = build_url subreddit, 'hot', max_results
        response = HTTParty.get url, :headers => { "User-Agent" => "any:api-test:v1 (by /u/noUsername)" }
        
        if response.code == 200
            JSON.parse(response.body)['data']['children'].map { 
                |value| value['data'].slice(
                    'author_fullname',
                    'title',
                    'created',
                    'author',
                    'num_comments',
                    'url',
                    'preview'
                )
            }
        else
            JSON.parse(response.body)
        end
    end

    def build_url subreddit, category, max_results
        "https://api.reddit.com/r/#{subreddit}/#{category}?limit=#{max_results}&raw_json=1"
    end
end