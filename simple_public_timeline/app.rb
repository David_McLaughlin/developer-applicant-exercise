require 'sinatra'
require 'sinatra/reloader' if development?
require 'liquid'
require './lib/reddit'
require './lib/liquid-render_raw'

configure do

  # Had to use Dir.pwd, __dir__ or __FILE__ in combiation with File.expand_path and File.dirname
  # would only every resovle to my root directory. Rather than dig into that Dir.pwd seems to work
  # so long as you run the app from the directory it's stored in.
  partials_path = File.join(Dir.pwd,'views/partials/')

  # Liquid::Template.file_system defaults to Liquid::BlankFileSystem which throws when trying to use the 
  # liquid 'render' or 'include' tag, all we need to do is tell liquid which directory to look in.
  Liquid::Template.file_system = Liquid::LocalFileSystem.new(partials_path)

  Liquid::Template.register_tag('render_raw', Liquid::Tags::RenderRawTag)
  set :public_folder, 'public'
end

get '/' do
  reddit = RedditAPI.new
  posts = reddit.hot "Twitter", 20
  users = posts.map { |value| value.slice('author_fullname') }

  # Some mock choices, for a production app we
  # would load these from some persistence layer
  languages = [ "First", "Second", "Third" ]

  liquid :index, :locals => {
    :site_name => 'Tweetaloo',
    :languages => languages,
    :post_collection => posts,
    :user_collection => users
  }
end

get '/via_js' do
  # Some mock choices, for a production app we
  # would load these from some persistence layer
  languages = [ "First", "Second", "Third" ]

  liquid :index, :locals => {
    :site_name => 'Tweetaloo',
    :languages => languages
  }
end

get '/style.css' do
  scss :styles
end

get '/api/v1/posts' do
  reddit = RedditAPI.new

  content_type 'application/json'
  (reddit.hot "Twitter", 20).to_json
end