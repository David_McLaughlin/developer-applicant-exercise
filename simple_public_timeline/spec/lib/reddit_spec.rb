require './lib/reddit'
require './spec/spec_helper.rb'

describe RedditAPI do
    it 'should build a valid url' do
        reddit      = RedditAPI.new
        subreddit   = 'all'
        category    = 'hot'
        max_results = 10

        url = reddit.build_url subreddit, category, max_results
        expected_url = "https://api.reddit.com/r/#{subreddit}/#{category}?limit=#{max_results}&raw_json=1"

        expect(url).to eq expected_url
    end
end