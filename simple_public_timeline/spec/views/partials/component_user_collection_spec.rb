require 'liquid'
require './spec/spec_helper.rb'

describe 'user_collection template' do
    it 'should be an empty div' do
        partials_path = File.join Dir.pwd,'views/partials/'
        Liquid::Template.file_system = Liquid::LocalFileSystem.new partials_path

        template = Liquid::Template.file_system.read_template_file "component_user_collection"
        rendered = Liquid::Template.parse(template).render(nil)

        expect(rendered).to eq "<div id=\"user-container\" class=\"user-container\">\n    \n</div>"
    end

    it 'should include input author_fullname' do
        partials_path = File.join Dir.pwd,'views/partials/'
        Liquid::Template.file_system = Liquid::LocalFileSystem.new partials_path

        template = Liquid::Template.file_system.read_template_file "component_user_collection"
        rendered = Liquid::Template.parse(template).render('user_collection' => [ 'author_fullname' => 'not-a-name' ])

        expect(rendered).to include "<span>not-a-name</span>"
    end
end